package ru.nlmk.sobolevmv.tm;

import java.util.Date;
import java.util.Scanner;
import static ru.nlmk.sobolevmv.tm.constant.TerminalConst.*;

import ru.nlmk.sobolevmv.tm.controller.*;
import ru.nlmk.sobolevmv.tm.enumerated.Role;
import ru.nlmk.sobolevmv.tm.repository.CommandRepository;
import ru.nlmk.sobolevmv.tm.repository.ProjectRepository;
import ru.nlmk.sobolevmv.tm.repository.TaskRepository;
import ru.nlmk.sobolevmv.tm.repository.UserRepository;
import ru.nlmk.sobolevmv.tm.service.*;

public class App {

  private final ProjectRepository projectRepository = new ProjectRepository();

  private final TaskRepository taskRepository = new TaskRepository();

  private final UserRepository userRepository = new UserRepository();

  private final CommandRepository commandRepository = new CommandRepository();

  private final ProjectService projectService = new ProjectService(projectRepository);

  private final TaskService taskService = new TaskService(taskRepository);

  private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

  private final UserService userService = new UserService(userRepository);

  private final CommandService commandService = new CommandService(commandRepository);

  private final ProjectController projectController = new ProjectController(projectService);

  private final TaskController taskController = new TaskController(taskService, projectTaskService);

  private final SystemController systemController = new SystemController();

  private final UserController userController = new UserController(userService);

  private final CommandController commandController = new CommandController(commandService);

  {
    userRepository.create("user1","111","Иванов","Иван","Иванович");
    userRepository.create("user2","222","Петров","Петр","Петрович");
    userRepository.create("user3","333","Сидоров","Сидор","Сидорович", Role.ADMIN);

    userService.userSignIn("user3","333");

    projectRepository.create("DEMO PROJECT1","", userService.getUserSignInId());
    projectRepository.create("DEMO PROJECT2","", userService.getUserSignInId());
    projectRepository.create("DEMO PROJECT3");

    taskRepository.create("TEST TASK1", "", userService.getUserSignInId());
    taskRepository.create("TEST TASK2", "", userService.getUserSignInId());
    taskRepository.create("TEST TASK3");
  }

  public static void main(final String[] args){
    final App app = new App();
    app.run(args);
    app.systemController.displayWelcome();
    app.process();
  }

  public void process(){
    final Scanner scanner = new Scanner(System.in);
    String command = "";
    while (!CMD_EXIT.equals(command)) {
      command = scanner.nextLine();
      run(command);
      System.out.println();
      commandController.createCommand(command, new Date());
    }
  }

  public void run (final String[] args){
    if (args == null) return;
    if (args.length < 1) return;
    final String param = args[0];
    final int  result = run(param);
    System.exit(result);
  }

  public int run (final String param){
    if (param == null || param.isEmpty()) return -1;
    switch (param) {
      case CMD_VERSION: return systemController.displayVersion();
      case CMD_ABOUT: return systemController.displayAbout();
      case CMD_HELP: return systemController.displayHelp ();
      case CMD_EXIT: return systemController.exit();
      case CMD_LIST: return commandController.listCommands();

      case CMD_PROJECT_CREATE: return projectController.createProject();
      case CMD_PROJECTS_CLEAR: return projectController.clearProject();
      case CMD_PROJECTS_LIST: return projectController.listProjects(userService.getUserSignInId());
      case CMD_PROJECT_VIEW_BY_INDEX: return projectController.viewProjectByIndex();
      case CMD_PROJECT_VIEW_BY_ID: return projectController.viewProjectById();
      case CMD_PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
      case CMD_PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
      case CMD_PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
      case CMD_PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();
      case CMD_PROJECT_UPDATE_BY_ID: return projectController.updateProjectById();

      case CMD_TASK_CREATE: return taskController.createTask();
      case CMD_TASKS_CLEAR: return taskController.clearTask();
      case CMD_TASKS_LIST: return taskController.listTasks(userService.getUserSignInId());
      case CMD_TASK_VIEW_BY_INDEX: return taskController.viewTaskByIndex();
      case CMD_TASK_VIEW_BY_ID: return taskController.viewTaskById();
      case CMD_TASK_REMOVE_BY_ID: return taskController.removeTaskById();
      case CMD_TASK_REMOVE_BY_NAME: return taskController.removeTaskByName();
      case CMD_TASK_REMOVE_BY_INDEX: return taskController.removeTaskByIndex();
      case CMD_TASK_UPDATE_BY_INDEX: return taskController.updateTaskByIndex();
      case CMD_TASK_UPDATE_BY_ID: return taskController.updateTaskById();
      case CMD_TASK_ADD_TO_PROJECT_BY_IDS: return taskController.addTaskToProjectByIds();
      case CMD_TASK_REMOVE_FROM_PROJECT_BY_IDS: return taskController.removeTaskFromProjectByIds();
      case CMD_TASKS_LIST_BY_PROJECT_ID: return taskController.listTasksByProjectId(userService.getUserSignInId());

      case CMD_USER_CREATE: return userController.createUser();
      case CMD_ADMIN_CREATE: return userController.createUser(Role.ADMIN);
      case CMD_USERS_CLEAR: return userController.clearUsers();
      case CMD_USERS_LIST: return userController.listUsers();
      case CMD_USER_VIEW_BY_LOGIN: return userController.viewUserByLogin();
      case CMD_USER_REMOVE_BY_LOGIN: return userController.removeUserByLogin();
      case CMD_USER_UPDATE_BY_LOGIN: return userController.updateUserByLogin();
      case CMD_USER_VIEW_BY_ID: return userController.viewUserById();
      case CMD_USER_REMOVE_BY_ID: return userController.removeUserById();
      case CMD_USER_UPDATE_BY_ID: return userController.updateUserById();
      case CMD_USER_SIGN_IN: return userController.userSignIn();
      case CMD_USER_SIGN_OUT: return userController.userSignOut();
      case CMD_USER_CHANGE_PASSWORD: return userController.userChangePassword();

      default: return systemController.displayError();
    }
  }

}


