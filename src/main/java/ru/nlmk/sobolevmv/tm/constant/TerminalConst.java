package ru.nlmk.sobolevmv.tm.constant;

public final class TerminalConst {

    public static final String CMD_HELP = "help";
    public static final String CMD_VERSION = "version";
    public static final String CMD_ABOUT = "about";
    public static final String CMD_EXIT = "exit";
    public static final String CMD_LIST = "command-list";

    public static final String CMD_PROJECT_CREATE = "project-create";
    public static final String CMD_PROJECTS_CLEAR = "projects-clear";
    public static final String CMD_PROJECTS_LIST = "projects-list";
    public static final String CMD_PROJECT_VIEW_BY_INDEX = "project-view-by-index";
    public static final String CMD_PROJECT_VIEW_BY_ID = "project-view-by-id";
    public static final String CMD_PROJECT_REMOVE_BY_NAME = "project-remove-by-e";
    public static final String CMD_PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    public static final String CMD_PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    public static final String CMD_PROJECT_UPDATE_BY_INDEX = "project-update-by-index";
    public static final String CMD_PROJECT_UPDATE_BY_ID = "project-update-by-id";

    public static final String CMD_TASK_CREATE = "task-create";
    public static final String CMD_TASKS_CLEAR = "tasks-clear";
    public static final String CMD_TASKS_LIST = "tasks-list";
    public static final String CMD_TASK_VIEW_BY_INDEX = "task-view-by-index";
    public static final String CMD_TASK_VIEW_BY_ID = "task-view-by-id";
    public static final String CMD_TASK_REMOVE_BY_NAME = "task-remove-by-name";
    public static final String CMD_TASK_REMOVE_BY_ID = "task-remove-by-id";
    public static final String CMD_TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    public static final String CMD_TASK_UPDATE_BY_INDEX = "task-update-by-index";
    public static final String CMD_TASK_UPDATE_BY_ID = "task-update-by-id";
    public static final String CMD_TASKS_LIST_BY_PROJECT_ID = "tasks-list-by-project-id";
    public static final String CMD_TASK_ADD_TO_PROJECT_BY_IDS = "task-add-to-project-by-ids";
    public static final String CMD_TASK_REMOVE_FROM_PROJECT_BY_IDS = "task-remove-form-project-by-ids";

    public static final String CMD_USER_CREATE = "user-create";
    public static final String CMD_ADMIN_CREATE = "admin-create";
    public static final String CMD_USERS_CLEAR = "users-clear";
    public static final String CMD_USERS_LIST = "users-list";
    public static final String CMD_USER_VIEW_BY_LOGIN = "user-view-by-login";
    public static final String CMD_USER_REMOVE_BY_LOGIN = "user-remove-by-login";
    public static final String CMD_USER_UPDATE_BY_LOGIN = "user-update-by-login";
    public static final String CMD_USER_VIEW_BY_ID = "user-view-by-id";
    public static final String CMD_USER_REMOVE_BY_ID = "user-remove-by-id";
    public static final String CMD_USER_UPDATE_BY_ID = "user-update-by-id";
    public static final String CMD_USER_SIGN_IN = "user-sign-in";
    public static final String CMD_USER_SIGN_OUT = "user-sign-out";
    public static final String CMD_USER_CHANGE_PASSWORD = "user-change-password";

}
